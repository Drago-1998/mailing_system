import datetime

from django.db.models import Subquery, OuterRef, Q, Count, Value, F
from django.db.models.functions import Replace, Coalesce, Now

from client.models import Client
from newsletter.models import Newsletter, NewsletterMassage
from newsletter.services import send_massage_service


def get_newsletters_with_report(filter_query: Q = Q()):
    newsletters = Newsletter.objects.filter(filter_query)
    clients_count_sq = Subquery(
        Client.objects.annotate(
            filter_args=OuterRef('filter_args')
        ).filter(
            Q(
                tag__iregex=Replace('filter_args', Value(','), Value('?|'))
            ) |
            Q(
                provider_code__iregex=Replace('filter_args', Value(','), Value('?|'))
            )
        ).values('filter_args').order_by('filter_args').annotate(
            count=Count('id')
        ).values('count')[:1]
    )
    newsletters = newsletters.annotate(
        clients_count=Coalesce(clients_count_sq, 0),
        amount_msgs_count=Count('massages__pk'),
        pending_msgs_count=Count('massages__pk', filter=Q(massages__status='pending')),
        sent_msgs_count=Count('massages__pk', filter=Q(massages__status='sent')),
        not_sent_msgs_count=Count('massages__pk', filter=Q(massages__status='not_sent')),
    )

    return newsletters


def get_clients_for_newsletter(newsletter: Newsletter) -> dict:
    _filter_args = newsletter.filter_args.replace(',', '?|')
    clients = Client.objects \
        .annotate(newsletter_massage_count=Count('massages__id',
                                                 filter=Q(massages__newsletter=newsletter))
                  ).filter(Q(tag__iregex=_filter_args) |
                           Q(provider_code__iregex=_filter_args),
                           Q(newsletter_massage_count=0)).in_bulk()

    return clients


def send_massages_logic():
    import pytz

    utc = pytz.UTC

    newsletters = get_newsletters_with_report(filter_query=Q(start_date__lt=Now(), end_date__gt=Now()))
    if not newsletters:
        return
    newsletters = newsletters.in_bulk()
    for newsletter_id, newsletter_obj in newsletters.items():
        clients = get_clients_for_newsletter(newsletter_obj)
        not_sent_massages = NewsletterMassage.objects \
            .annotate(client_phone_number=F('client__phone_number')) \
            .filter(newsletter_id=newsletter_id,
                    status__in=['not_sent', 'pending']) \
            .in_bulk()
        update_massages = []
        for client_id, client_obj in clients.items():
            end_date = newsletter_obj.end_date
            _now = utc.localize(datetime.datetime.now())
            if end_date < _now:
                break
            newsletter_massage = NewsletterMassage.objects.create(
                status='pending',
                newsletter_id=newsletter_id,
                client_id=client_id
            )
            response_code, massage = send_massage_service(massage_id=newsletter_massage.id,
                                                          text=newsletter_obj.content,
                                                          phone_number=client_obj.phone_number)
            if response_code == 200:
                newsletter_massage.status = 'sent'
                update_massages.append(newsletter_massage)
            else:
                newsletter_massage.status = 'not_sent'
                update_massages.append(newsletter_massage)

        for newsletter_massage_id, newsletter_massage_obj in not_sent_massages.items():
            end_date = newsletter_obj.end_date
            _now = utc.localize(datetime.datetime.now())
            if end_date < _now:
                break
            response_code, massage = send_massage_service(massage_id=newsletter_massage_id,
                                                          text=newsletter_obj.content,
                                                          phone_number=newsletter_massage_obj.client_phone_number)
            if response_code == 200:
                newsletter_massage_obj.status = 'sent'
                update_massages.append(newsletter_massage_obj)
            elif newsletter_massage_obj != 'not_sent':
                newsletter_massage_obj.status = 'not_sent'
                update_massages.append(newsletter_massage_obj)

        NewsletterMassage.objects.bulk_update(update_massages, fields=('status',), batch_size=5000)
