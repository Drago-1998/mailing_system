from django.contrib import admin
from django.contrib.admin import display

from newsletter.logic import get_newsletters_with_report
from newsletter.models import Newsletter, NewsletterMassage


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = 'id', 'start_date', 'content', 'filter_args', 'end_date', \
                   'get_clients_count', 'get_pending_msgs_count', 'get_sent_msgs_count', \
                   'get_not_sent_msgs_count',

    def get_queryset(self, request):
        return get_newsletters_with_report()

    @display(ordering='clients_count', description='Количество клиентов')
    def get_clients_count(self, obj):
        return obj.clients_count

    @display(ordering='pending_msgs_count', description='Сообшение в очереде')
    def get_pending_msgs_count(self, obj):
        return obj.pending_msgs_count

    @display(ordering='sent_msgs_count', description='Сообшение отправлено')
    def get_sent_msgs_count(self, obj):
        return obj.sent_msgs_count

    @display(ordering='not_sent_msgs_count', description='Сообшение не отправлено')
    def get_not_sent_msgs_count(self, obj):
        return obj.not_sent_msgs_count


@admin.register(NewsletterMassage)
class NewsletterMassageAdmin(admin.ModelAdmin):
    list_display = 'id', 'created_at', 'updated_at', 'status', 'newsletter'
