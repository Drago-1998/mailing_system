from django.db.models import Q
from django.db.models.functions import Now

from mailing_system.celery import app
from newsletter.logic import get_newsletters_with_report, send_massages_logic


@app.task
def send_start():

    """ Запуск задачи по отправки сообшение """
    send_massages_logic()
