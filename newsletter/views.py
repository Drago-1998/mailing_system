from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters

from client.paginator import SmallCustomPagination
from newsletter.logic import get_newsletters_with_report
from newsletter.models import Newsletter
from newsletter.serializers import NewsletterSerializer


class NewsletterViewSet(viewsets.ModelViewSet):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer
    pagination_class = SmallCustomPagination
    filter_backends = (
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    )

    def get_queryset(self):
        return get_newsletters_with_report()
